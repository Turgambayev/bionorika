<p style="padding:0;Margin:0;padding-top:20px;">Кто-то запросил сброс пароля</p>
<p style="padding:0;Margin:0;padding-top:15px;">Если это был не вы, не обращайте внимания на это письмо. <br>Только никому не говорите код!</p>
<p style="padding:0;Margin:0;padding-top:20px;">Код для сброса пароля:&nbsp;</p>
<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:24px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:36px;color:#333333;">{{ $code }}</p>

