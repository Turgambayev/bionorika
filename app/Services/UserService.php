<?php


namespace App\Services;


use App\DoctorScore;
use App\DoctorTest;
use App\PharmacistScore;
use App\UserProfile;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class UserService
{
    public function getPharmacistScores($list)
    {
        $scores = PharmacistScore
            ::query()
            ->join('user_profiles as u_p', function (JoinClause $join) {
                $join->on('pharmacist_scores.user_id', '=', 'u_p.user_id')
                    ->where('profession_id', UserProfile::isPharmacist);
            })
            ->join('pharmacist_tests as p_t', 'pharmacist_scores.test_id', '=', 'p_t.id')
            ->join('users as u', 'u_p.user_id', '=', 'u.id')
            ->selectRaw(
                'u.id as u_id,
                p_t.id as p_t_id,
                max(u.name) as u_name,
                max(u.email) as u_email,
                max(u_p.place_of_work) as u_place_of_work,
                max(u_p.city) as u_city,
                max(p_t.name) as p_t_name,
                max(pharmacist_scores.score) as score,
                max(pharmacist_scores.created_at) as created_at'
            )
            ->groupBy(['u_id', 'p_t_id'])
            ->get();

        foreach ($scores as $score) {
            $list[] = [
                'Идентификатор' => $score['u_id'],
                'ФИО' => $score['u_name'],
                'Электронная почта' => $score['u_email'],
                'Место работы' => $score['u_place_of_work'],
                'Город' => $score['u_city'],
                'Наименование теста' => $score['p_t_name'],
                'Количество попыток' => PharmacistScore::query()
                    ->where('user_id', $score['u_id'])
                    ->where('test_id', $score['p_t_id'])
                    ->count(),
                'Максимальный балл' => $score['score'],
                'Дата прохождения теста' => (string) $score['created_at'],
            ];
        }

        return $list;
    }

    public function getDoctorScores($list)
    {
        $scores = DoctorScore
            ::query()
            ->join('user_profiles as u_p', function (JoinClause $join) {
                $join->on('doctor_scores.user_id', '=', 'u_p.user_id')
                    ->where('profession_id', UserProfile::isDoctor)
                    ->whereIn('direction_id',
                        DoctorTest::query()
                            ->join('doctor_video_lesson_directions as dvld', 'dvld.doctor_video_lesson_id',
                                '=', 'doctor_tests.doctor_video_lesson_id')
                            ->pluck('dvld.direction_id')
                            ->toArray());
            })
            ->join('doctor_tests as d_t', 'doctor_scores.test_id', '=', 'd_t.id')
            ->join('users as u', 'u_p.user_id', '=', 'u.id')
            ->selectRaw(
                'u.id as u_id,
                max(u.name) as u_name,
                max(u.email) as u_email,
                max(u_p.place_of_work) as u_place_of_work,
                max(u_p.city) as u_city,
                d_t.id as d_t_id,
                max(d_t.name) as d_t_name,
                max(doctor_scores.score) as score,
                max(doctor_scores.created_at) as created_at'
            )
            ->groupBy(['u_id', 'd_t_id'])
            ->get();

        foreach ($scores as $score) {
            $count = 0;
            if(isset($certificate[$score['u_id']])){
                $certificates = json_decode($certificate[$score['u_id']]->certificate);
                if($certificates){
                    $count = count($certificates);
                }
            }

            $list[] = [
                'Идентификатор' => $score['u_id'],
                'ФИО' => $score['u_name'],
                'Электронная почта' => $score['u_email'],
                'Место работы' => $score['u_place_of_work'],
                'Город' => $score['u_city'],
                'Наименование теста' => $score['d_t_name'],
                'Количество попыток' => DoctorScore::query()
                    ->where('user_id', $score['u_id'])
                    ->where('test_id', $score['d_t_id'])
                    ->count(),
                'Максимальный балл' => $score['score'],
                'Дата прохождения теста' => $score['created_at'],
                'Сертификаты (в текущем цикле)' => isset($certificate[$score['u_id']]) ? "Загружено" : "Не загружено",
                'Количество' => $count,
            ];
        }

        return $list;
    }
}
