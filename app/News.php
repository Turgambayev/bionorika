<?php

namespace App;

use App\Helpers\TranslatesCollection;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;



class News extends Model
{
    use Translatable;
    protected $translatable = ['name', 'short_description', 'full_description', 'meta_title', 'meta_description', 'meta_keyword'];

    public static function getAll(){
        $model = self::orderBy('sort', 'ASC')->get();
        foreach ($model as $v) $v->image = Voyager::image($v->image);
        return $model;
    }


    public static function search($keyword){

        if(app()->getLocale() == 'ru') {
            $model = self::where('name', 'LIKE', "%{$keyword}%")
                ->orWhere('short_description', 'LIKE', "%{$keyword}%")
                ->select('id', 'name', 'short_description')
                ->get();
        }else {
            $model = self::whereTranslation('short_description', 'LIKE', "%{$keyword}%", [app()->getLocale()], false)
                ->select('id', 'name', 'short_description')
                ->get();
        }

        return $model;
    }


}
