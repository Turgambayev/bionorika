<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.05.2020
 * Time: 13:02
 */

namespace App\Traits;


trait Scorable
{
    public function createNew($user_id, $test_id, $score){
        return $this::create([
            'user_id' => $user_id,
            'test_id' => $test_id,
            'score' => $score
        ]);
    }

    public static function getTestAttempt($user_id, $test_id){
        return self::where([['user_id', $user_id], ['test_id', $test_id]])->count();
    }

    public static function getUserTests($user_id, $test_id = null){

        if($test_id != null){
            return self::where([['user_id', $user_id], ['test_id', $test_id]])->get();
        }else{
            return self::where(['user_id' => $user_id])->get();
        }
    }

    public static function getMaxBall($user_id, $test_id){
        return self::where([['user_id', $user_id], ['test_id', $test_id]])->max('score');
    }

    public static function getMaxBallTest($user_id, $test_id){
        return self::where([['user_id', $user_id], ['test_id', $test_id], ])->orderByDesc('score')->first();
    }
}
