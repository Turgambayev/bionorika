<?php

namespace App;

use App\Helpers\TranslatesCollection;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Facades\Voyager;


class Product extends Model
{
    use Translatable;
    protected $translatable = ['name', 'description', 'indication_for_use', 'drug_properties', 'composition',
        'dosage', 'instruction_for_use', 'faq', 'meta_title', 'meta_description', 'meta_keyword'];

    public static function getAll($catalog_id){

        $model = self::where('catalog_id', $catalog_id)
            ->orderBy('sort', 'ASC')
            ->select('id', 'name', 'image')
            ->get();

        foreach ($model as $v) {
            $v->image = Voyager::image($v->image);
        }

        return $model;
    }


    public static function search($keyword){

        if(app()->getLocale() == 'ru') {
            $model = self::where('name', 'LIKE', "%{$keyword}%")
                ->orWhere('description', 'LIKE', "%{$keyword}%")
                ->select('id', 'name', 'description', 'image')
                ->get();
        }else {
            $model = self::whereTranslation('name', 'LIKE', "%{$keyword}%", [app()->getLocale()], false)
                ->select('id', 'name', 'description', 'image')
                ->get();
        }

        foreach ($model as $v) {
            $v->image = Voyager::image($v->image);
        }

        return $model;
    }


    public function documents(){
        return $this->hasMany('App\Document', 'product_id', 'id');
    }



}

