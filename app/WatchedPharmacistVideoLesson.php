<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WatchedPharmacistVideoLesson extends Model
{

    protected $fillable = [
        'user_id', 'lesson_id'
    ];

    public function createNew($user_id, $lesson_id){

        return self::create([
            'user_id' => $user_id,
            'lesson_id' => $lesson_id,
        ]);
    }


    public static function exists($user_id, $lesson_id){
        return self::where([['user_id', $user_id],['lesson_id', $lesson_id]])->exists();
    }

    public static function getAll($user_id){

        $model = self::where('user_id', $user_id)->get();
        $arr = [];
        foreach ($model as $v){
            $arr[] = $v->lesson_id;
        }

        return PharmacistVideoLesson::getAll($arr);
    }

    public static function deleteByUser($user_id){
        return self::where('user_id', $user_id)->delete();
    }
}
