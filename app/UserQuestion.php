<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class UserQuestion extends Model
{
    protected $fillable = [
        'user_id', 'answer', 'question_id'
    ];

    public static function getAnswersByID($id){
        return self::where('user_id', $id)->get();
    }

    public function question(){
        return $this->hasOne('App\Question', 'id', 'question_id');
    }

    public function questionText(){
        return $this->question ? $this->question->content : "Не указано";
    }

    public function createNew($question_id, $answer, $user_id){
        return UserQuestion::create([
            'question_id' => $question_id,
            'answer' => $answer,
            'user_id' => $user_id
        ]);
    }
}
