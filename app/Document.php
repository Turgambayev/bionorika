<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Document extends Model
{
    use Translatable;
    protected $translatable = ['name'];

    public static function getAllByProduct($product_id){
        return self::where('product_id', $product_id)->get();
    }
}
