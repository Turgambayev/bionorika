<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class DoctorAssessmentPrinciple extends Model
{
    use Translatable;
    protected $translatable = ['general', 'after_result'];
}
