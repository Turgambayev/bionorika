<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'description' => $this->description,
            'indication_for_use' => $this->indication_for_use,
            'drug_properties' => $this->drug_properties,
            'composition' => $this->composition,
            'dosage' => $this->dosage,
            'instruction_for_use' => $this->instruction_for_use,
            'faq' => $this->faq,
            'documents' => $this->documents,
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'meta_keyword' => $this->meta_keyword
        ];
    }
}
