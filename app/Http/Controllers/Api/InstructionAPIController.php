<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Instruction;
use App\Http\Resources\InstructionResource;
use App\Http\Controllers\Controller;

class InstructionAPIController extends Controller
{
    public function index()
    {
        $instruction = Instruction::getContent();
        TranslatesCollection::translate($instruction, app()->getLocale());
        return new InstructionResource($instruction);
    }

}
