<?php

namespace App\Http\Controllers\API;

use App\Certificate;
use App\Http\Resources\CertificateCollection;
use App\Traits\Cyclable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;
use App\Http\Controllers\Api\ScaleAPIController;



class CertificateAPIController extends ScaleAPIController
{
    use Cyclable;

    public function index()
    {
        $certificates = $this->getCertificates(Auth::id());
        return response()->json(['data' => $certificates],200);
    }


    public function destroy(Request $request, Certificate $certificate)
    {
        $validateArray = [
            'order_id' => 'required|numeric',
        ];

        if($userValidate = $this->check($request, $validateArray)) {
            return $userValidate;
        }

        $model = Certificate::getByUser(Auth::id(), $this->getMonthsAsArray());
        $order_id = $request->order_id;
        if(count(json_decode($model->certificate)) >= $order_id) {

            $this->deleteCertificate($model, $order_id);
            $data = $this->getCertificates(Auth::id());

            return response()->json(['data' => $data],200);
        }else{
            return response(['Network does not exist'], 422);
        }
    }



    public function store(Request $request)
    {

        $current = Certificate::getByUser(Auth::id(), $this->getMonthsAsArray());
        if($current && count(json_decode($current->certificate)) < 2 || is_null($current)) {
            $validateArray = [
                'file' => 'required|file',
            ];

            if ($userValidate = $this->check($request, $validateArray)) {
                return $userValidate;
            }

            $file = $request->file('file');
            $path = 'certificates\\' . Carbon::now()->format('F') . '' . Carbon::now()->year;

            $download_link = Storage::disk(config('voyager.storage.disk'))->put($path, $file);
            $original_name = $file->getClientOriginalName();
            $certificate = [];

            $certificates = Certificate::getByUser(Auth::id(), $this->getMonthsAsArray());
            if ($certificates) {
                if ($certificates->addNewFile(Auth::id(), $download_link, $original_name)) {
                    $certificate = $this->getLastFile(Auth::id());
                }
            } else {
                $model = new Certificate();
                if ($model->createNew(Auth::id(), $download_link, $original_name)) {
                    $certificate = $this->getLastFile(Auth::id());
                }
            }

            return new CertificateCollection($certificate);
        }else{
            return response(['You cannot upload anymore'], 422);
        }

    }



    private function getLastFile($user_id){

        $model = Certificate::getByUser($user_id, $this->getMonthsAsArray());
        $index = count(json_decode($model->certificate))-1;

        $data[] = [
            'order_id' => $index+1,
            'name' => json_decode($model->certificate)[$index]->original_name,
            'certificate' => Voyager::image(json_decode($model->certificate)[$index]->download_link)
        ];

        return $data;
    }


    private function getCertificates($user_id){

        $data = [];
        $months = $this->getMonths();
        for($year = self::fromYear;$year <= Carbon::now()->year;$year++){
            $m = 0;
            $cycle = [];
            foreach ($months as $k => $month){
                $m++;
                $model = Certificate::getByUserYearAndMonth($user_id, $year, $month);
                $certificate = null;
                if($model != null){
                    for ($i = 0;$i < count(json_decode($model->certificate));$i++){
                        $certificate[] = [
                            'order_id' => $i+1,
                            'name' => json_decode($model->certificate)[$i]->original_name,
                            'certificate' => Voyager::image(json_decode($model->certificate)[$i]->download_link)
                        ];
                    }
                }

                $cycle[] = [
                    'title' => $m.' '.trans('messages.цикл'),
                    'certificates' => $certificate ,
                ];
            }


            $data[] = [
                'year' => $year,
                'cycles' => $cycle
            ];
        }

        $current = Certificate::getByUser($user_id, $this->getMonthsAsArray());
        if($current && count(json_decode($current->certificate)) >= 2){
            $status = 0;
        }else{
            $status = 1;
        }

        $certificates = [
            'status' => $status,
            'certificates' => $data
        ];

        return $certificates;
    }


    private function deleteCertificate($model, $order_id){

        if($model != null){

            $key = $order_id - 1;
            $fieldData = json_decode($model->certificate);

            if(isset($fieldData[$key])) {

                $fileToRemove = $fieldData[$key]->download_link ?? $fieldData[$key];
                unset($fieldData[$key]);

                Storage::disk(config('voyager.storage.disk'))->delete($fileToRemove);
                $model->certificate = empty($fieldData) ? null : json_encode(array_values($fieldData));

                $model->save();
            }
        }

    }
}
