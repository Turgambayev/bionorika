<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.04.2020
 * Time: 17:06
 */

namespace App\Http\Controllers\API;

use App\DoctorVideoLesson;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\PharmacistVideoLesson;
use App\UserProfile;
use App\WatchedDoctorVideoLesson;
use App\WatchedPharmacistVideoLesson;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class WatchedVideoAPIController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        $profile = UserProfile::getProfileByID($id);

        if ($profile->profession_id == UserProfile::isDoctor) {
            $videoLesson = WatchedDoctorVideoLesson::getAll($id, $profile->direction_id);
        } elseif ($profile->profession_id == UserProfile::isPharmacist) {
            $videoLesson = WatchedPharmacistVideoLesson::getAll($id);
        } else {
            return response(['Network does not exist'], 422);
        }

        TranslatesCollection::translate($videoLesson, app()->getLocale());
        return response()->json(['data' => $videoLesson], 200);
    }


    public function update(Request $request)
    {

        $validateArray = ['lesson_id' => 'required'];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }

        $user = Auth::user();
        $profile = UserProfile::getProfileByID($user->id);
        $lesson_id = $request->lesson_id;

        if($profile->profession_id == UserProfile::isDoctor) {

            if (DoctorVideoLesson::exists($lesson_id) && !WatchedDoctorVideoLesson::exists($user->id, $lesson_id)){
                $watchedVideoLesson = new WatchedDoctorVideoLesson();
                $data = $watchedVideoLesson->createNew($user->id, $lesson_id);
            }else{
                return response(['Network does not exist'], 422);
            }

        }elseif($profile->profession_id == UserProfile::isPharmacist){

            if (PharmacistVideoLesson::exists($lesson_id) && !WatchedPharmacistVideoLesson::exists($user->id, $lesson_id)){
                $watchedPharmacistLesson = new WatchedPharmacistVideoLesson();
                $data = $watchedPharmacistLesson->createNew($user->id, $lesson_id);
            }else{
                return response(['Network does not exist'], 422);
            }

        }else{
            return response(['Network does not exist'], 422);
        }

        return response()->json(['data' => $data]);

    }



}

