<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 07.04.2020
 * Time: 13:07
 */

namespace App\Http\Controllers\API;

use App\DoctorAssessmentPrinciple;
use App\DoctorQuestionnaire;
use App\DoctorTestQuestion;
use App\PharmacistAssessmentPrinciple;
use App\PharmacistScore;
use App\PharmacistScoreType;
use App\PharmacistTest;
use App\PharmacistTestQuestion;
use App\PharmacistVideoLesson;
use App\DoctorScore;
use App\DoctorScoreType;
use App\Questionnaire;
use App\Traits\Cyclable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\DoctorTest;
use App\DoctorVideoLesson;
use App\Helpers\TranslatesCollection;
use App\Http\Controllers\Controller;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;

class TestAPIController extends Controller
{
    use Cyclable;

    public function index(Request $request)
    {
        $validateArray = ['lesson_id' => 'required'];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }

        $user = Auth::user();
        $profile = UserProfile::getProfileByID($user->id);
        $lesson_id = $request->lesson_id;

        if ($profile->profession_id == UserProfile::isDoctor) {

            if(DoctorVideoLesson::exists($lesson_id) && DoctorTest::getStatus($lesson_id)){
                $test = DoctorTest::getContent($user->id, $lesson_id);
            }else {
                return response(['Network does not exist'], 422);
            }

        } elseif ($profile->profession_id == UserProfile::isPharmacist) {
            if(PharmacistVideoLesson::exists($lesson_id) && PharmacistTest::getStatus($lesson_id)){
                $test = PharmacistTest::getContent($user->id, $lesson_id);
            }else {
                return response(['Network does not exist'], 422);
            }
        } else {
            return response(['Network does not exist'], 422);
        }

        TranslatesCollection::translate($test, app()->getLocale());
        return response()->json(['data' => $test], 200);
    }




    public function start(Request $request)
    {
        $validateArray = ['test_id' => 'required'];

        if($errors = $this->check($request, $validateArray)) {
            return $errors;
        }

        $user = Auth::user();
        $profile = UserProfile::getProfileByID($user->id);
        $test_id = $request->test_id;

        if ($profile->profession_id == UserProfile::isDoctor && ($test = DoctorTest::find($test_id)) != null) {
            if($test != null && DoctorVideoLesson::exists($test->doctor_video_lesson_id) && DoctorTest::exists($test_id) &&
                DoctorTest::getStatus($test->doctor_video_lesson_id) && DoctorScore::checkAttempt($user->id, $test_id)){
                if(!DoctorScore::testAlreadyStarted($user->id, $test_id)){
                    $time = $test->test_time;
                    $test = DoctorTestQuestion::getQuestions($test_id);
                    $userScore = new DoctorScore();
                    $score = $userScore->createNew($user->id, $test_id, null);
                }else{
                    return response(['Test already started'], 422);
                }

            }else {
                return response(['Network does not exist'], 422);
            }


        } elseif ($profile->profession_id == UserProfile::isPharmacist && ($test = PharmacistTest::find($test_id)) != null) {
            if($test != null && PharmacistVideoLesson::exists($test->pharmacist_video_lessont_id) && PharmacistTest::exists($test_id) &&
                PharmacistTest::getStatus($test->pharmacist_video_lessont_id) && PharmacistScore::checkAttempt($user->id, $test_id)){
                if(!PharmacistScore::testAlreadyStarted($user->id, $test_id)){
                    $time = $test->test_time;
                    $test = PharmacistTestQuestion::getQuestions($test_id);
                    $userScore = new PharmacistScore();
                    $score = $userScore->createNew($user->id, $test_id, 0);
                }else{
                    return response(['Test already started'], 422);
                }
            }else {
                return response(['Network does not exist'], 422);
            }

        } else {
            return response(['Network does not exist'], 422);
        }


        $data['attempt_id'] = $score->id;
        $data['time'] = $time;
        $data['tests'] = $test;
        return response()->json(['data' => $data], 200);
    }


    public function finish(Request $request){

        $validateArray = ["attempt_id" => "required"];
        if($errors = $this->check($request, $validateArray)) {return $errors;}

        $answers = $request->answers;
        $attempt_id = $request->attempt_id;

        $profile = UserProfile::getProfileByID(Auth::id());

        if ($profile->profession_id == UserProfile::isDoctor &&  DoctorScore::find($attempt_id) && !DoctorScoreType::check($attempt_id)) {

            $score = DoctorScore::find($attempt_id);
            if(!DoctorScore::checkStatus($score->id, DoctorTest::find($score->test_id)))  return response(['Passing test time passed'], 422);
            $questions = DoctorTestQuestion::getQuestions($score->test_id);
            if($errors = $this->checkAndValidateDoctorQuestions($answers, $questions)) return $errors;

            foreach ($answers as $k => $v){
                $answer_id = isset($v['answer_id']) ? $v['answer_id'] : null;
                $own_answer = isset($v['own_answer']) ? $v['own_answer'] : null;
                $userQuestion = new DoctorScoreType();
                $userQuestion->createNew($attempt_id, $v['question_id'], $answer_id, $own_answer);
            }

            $score->update([
                'passed_at' => Carbon::now() ]);
            $title = trans('messages.ready');
            $content = trans('messages.soon the results will be come out');
            $attempts_left = DoctorTest::getAttempt($score->test_id) - DoctorScore::getTestAttempt(Auth::id(), $score->test_id);
            $questionnaire = $this->getDoctorQuestionnaire(Auth::id(), $score->test_id);

        } elseif ($profile->profession_id == UserProfile::isPharmacist &&  PharmacistScore::find($attempt_id) && !PharmacistScoreType::check($attempt_id)) {

            $score = PharmacistScore::find($attempt_id);
            if(!PharmacistScore::checkStatus($score->id, PharmacistTest::find($score->test_id)))  return response(['Passing test time passed'], 422);
            $questions = PharmacistTestQuestion::getQuestions($score->test_id);
            if($errors = $this->checkAndValidatePharmacistQuestions($answers, $questions)) return $errors;

            foreach ($answers as $k => $v){
                $is_correct = PharmacistTestQuestion::check($v['question_id'], $v['answer_id']);
                $userQuestion = new PharmacistScoreType();
                $userQuestion->createNew($attempt_id, $v['question_id'], $v['answer_id'], $is_correct);
            }

            $correct = PharmacistScoreType::getCorrectCount($attempt_id);
            $questionCount = PharmacistTestQuestion::getCount($score->test_id);
            $correctPercent = intval(($correct * 100) / $questionCount);

            $score->update([
                'score' => $correct,
                'passed_at' => Carbon::now()
            ]);

            $title = $this->getTestResultTextForPharmacist($correctPercent);
            $content = trans('messages.your score', ['correct' => $correct, 'quantity' => $questionCount]);
            $attempts_left = PharmacistTest::getAttempt($score->test_id) - PharmacistScore::getTestAttempt(Auth::id(), $score->test_id);
            $questionnaire = null;

        }else {
            return response(['Network does not exist'], 422);
        }

        if($questionnaire != null){
            $data = [
                'title' => $title,
                'content' => $content,
                'attempts_left' => $attempts_left,
                'questionnaire_status' => 1,
                'test_id' => $score->test_id
            ];
        }else{
            $data = [
                'title' => $title,
                'content' => $content,
                'attempts_left'=> $attempts_left,
                'questionnaire_status' => 0
            ];
        }

        return response()->json(['data' => $data], 200);
    }


    protected function getDoctorQuestionnaire($user_id, $test_id){

        if(!DoctorQuestionnaire::alreadyExist($test_id, $user_id) && DoctorTest::where('id', $test_id)->exists()){
             $questionnaire = Questionnaire::getCurrentCycleData($this->getCurrentCycle());
             if(count($questionnaire) > 0){
                 return $questionnaire;
             }
        }

        return null;
    }



    private function checkAndValidateDoctorQuestions($answers, $questions){

        $errors = [];  $m = 0;
        $answerOption = [1, 2, 3];

        foreach ($questions as $question) {
            $statusQuestion = 0;$statusAnswer = 0;$m++;
            if($answers != null) {
                foreach ($answers as $answer) {
                    if (isset($answer['question_id']) && $question->id == $answer['question_id']) {
                        $statusQuestion = 1;
                        if ((isset($answer['answer_id']) && !isset($answer['own_answer']) && in_array($answer['answer_id'], $answerOption)) ||
                            (!isset($answer['answer_id']) && isset($answer['own_answer']))) $statusAnswer = 1;
                    }
                }
            }

            if ((!$statusQuestion && !$statusAnswer) || (!$statusQuestion && $statusAnswer)) {
                $errors["error"][$question->id] = [
                    "question" => trans('messages.question is required', ['number' => $m]),
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            } elseif ($statusQuestion && !$statusAnswer) {
                $errors["error"][$question->id] = [
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            }

        }

        return $errors;
    }



    private function checkAndValidatePharmacistQuestions($answers, $questions){

        $errors = []; $m = 0;
        $answerOption = [1, 2, 3, 4];
        foreach ($questions as  $question) {
            $statusQuestion = 0; $statusAnswer = 0; $m++;

            if($answers != null) {
                foreach ($answers as $answer) {
                    if (isset($answer['question_id']) && $question->id == $answer['question_id']) {
                        $statusQuestion = 1;
                        if (isset($answer['answer_id']) && in_array($answer['answer_id'], $answerOption)) $statusAnswer = 1;
                    }
                }
            }

            if((!$statusQuestion && !$statusAnswer)) {
                $errors["error"][$question->id] = [
                    "question" => trans('messages.question is required', ['number' => $m]),
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            }elseif(!$statusAnswer){
                $errors["error"][$question->id] = [
                    "answer" => trans('messages.answer is required', ['number' => $m])];
            }
        }

        return $errors;
    }





    public function receivedPoints(){

        $profile = UserProfile::getProfileByID(Auth::ID());
        if ($profile->profession_id == UserProfile::isDoctor) {

            $tests = DoctorTest::getTestByDirection($profile->direction_id);
            TranslatesCollection::translate($tests, app()->getLocale());
            $data = $this->getReceivedPointsForDoctor(Auth::ID(), $tests);
        } elseif ($profile->profession_id == UserProfile::isPharmacist) {

            $tests = PharmacistTest::get();
            TranslatesCollection::translate($tests, app()->getLocale());
            $data = $this->getReceivedPointsForPharmacist(Auth::ID(), $tests);
        } else {
            return response(['Network does not exist'], 422);
        }

        return response()->json(['data' => $data], 200);
    }




    public function passedTests(){
        $profile = UserProfile::getProfileByID(Auth::ID());
        if ($profile->profession_id == UserProfile::isDoctor) {

            $tests = DoctorTest::getTestByDirection($profile->direction_id);
            TranslatesCollection::translate($tests, app()->getLocale());
            $data = $this->getPassedTestsForDoctor(Auth::ID(), $tests);
        } elseif ($profile->profession_id == UserProfile::isPharmacist) {

            $tests = PharmacistTest::get();
            TranslatesCollection::translate($tests, app()->getLocale());
            $data = $this->getPassedTestsForPharmacist(Auth::ID(), $tests);
        } else {
            return response(['Network does not exist'], 422);
        }

        return response()->json(['data' => $data], 200);
    }






    public function getPassedTestsForDoctor($user_id, $tests){

        $data = [];
        foreach ($tests as $k => $test) {
            if (DoctorScore::getTestAttempt($user_id, $test->id) > 0){

                $max_ball = DoctorScore::getMaxBall($user_id, $test->id);
                $questionCount = DoctorTestQuestion::getCount($test->id);
                $correctPercent = intval(($max_ball * 100) / $questionCount);

                if($max_ball != null) {
                    $data[] = [
                        'name' => $test->name,
                        'lesson_id' => $test->doctor_video_lesson_id,
                        'max_ball' => $max_ball,
                        'status' => $this->getTestTextForDoctor($correctPercent),
                        'button_status' => ($test->attempts - DoctorScore::getTestAttempt(Auth::id(), $test->id)) > 0 ? 1 : 0
                    ];
                }else{
                    $data[] = [
                        'name' => $test->name,
                        'lesson_id' => $test->doctor_video_lesson_id,
                        'max_ball' => $max_ball,
                        'status' => 'На проверке',
                        'button_status' => ($test->attempts - DoctorScore::getTestAttempt(Auth::id(), $test->id)) > 0 ? 1 : 0
                    ];
                }
            }else{
                $data[] = [
                    'name' => trans('messages.not available'),
                ];
            }
        }

        return $data;
    }



    public function getPassedTestsForPharmacist($user_id, $tests){

        $data = [];
        foreach ($tests as $k => $test){
            if (PharmacistScore::getTestAttempt($user_id, $test->id) > 0){

                $max_ball = PharmacistScore::getMaxBall($user_id, $test->id);
                $questionCount = PharmacistTestQuestion::getCount($test->id);
                $correctPercent = intval(($max_ball * 100) / $questionCount);

                $data[] = [
                    'lesson_id' => $test->pharmacist_video_lessont_id,
                    'name' => $test->name,
                    'max_ball' => $max_ball,
                    'status' => $this->getTestTextForPharmacist($correctPercent),
                    'button_status' => ($test->attempts - PharmacistScore::getTestAttempt(Auth::id(), $test->id)) > 0 ? 1 : 0
                ];
            }else{
                $data[] = [
                    'name' => trans('messages.not available')
                ];
            }
        }


        return $data;
    }



    private function getReceivedPointsForDoctor($user_id, $tests){

        $data = [];
        foreach ($tests as $k => $test){

            $attempts = DoctorScore::getUserTests($user_id, $test->id);
            $questionCount = DoctorTestQuestion::getCount($test->id);

            $m=0;
            $df = [];

            foreach ($attempts as $attempt){
                $m++;
                $correctPercent = intval(($attempt->score * 100) / $questionCount);
                if($attempt->score != null){
                    $df[] = [
                        'attempt' => trans('messages.attempt').''. $m,
                        'status' => $this->getTestTextForDoctor($correctPercent),
                        'score' => $attempt->score
                    ];
                }else{
                    $df[] = [
                        'attempt' => trans('messages.attempt').''. $m,
                        'status' => 'На проверке',
                        'score' => ''
                    ];
                }
            }

            for ($i = 0; $i < $test->attempts-count($attempts); $i++){
                $m++;
                $df[] = [
                    'attempt' => trans('messages.attempt').''. $m,
                    'status' => "",
                    'score' =>  trans('messages.not used')
                ];
            }
            $data[] = [
                'name' => $test->name,
                'attempts' => $df
            ];
        }

        return $data;
    }




    private function getReceivedPointsForPharmacist($user_id, $tests){

        $data = [];
        foreach ($tests as $k => $test){

            $attempts = PharmacistScore::getUserTests($user_id, $test->id);
            $questionCount = PharmacistTestQuestion::getCount($test->id);

            $m=0;
            $df = [];

            foreach ($attempts as $attempt){
                $m++;
                $correctPercent = intval(($attempt->score * 100) / $questionCount);
                $df[] = [
                    'attempt' => trans('messages.attempt').''. $m,
                    'status' => $this->getTestTextForPharmacist($correctPercent),
                    'score' => $attempt->score
                ];
            }

            for ($i = 0; $i < $test->attempts-count($attempts); $i++){
                $m++;
                $df[] = [
                    'attempt' => trans('messages.attempt').''. $m,
                    'status' => "",
                    'score' =>  trans('messages.not used')
                ];
            }
            $data[] = [
                'name' => $test->name,
                'attempts' => $df
            ];
        }

        return $data;
    }




    public function getTestTextForPharmacist($correctPercent){

        $model = PharmacistAssessmentPrinciple::get();
        TranslatesCollection::translate($model, app()->getLocale());
        return self::getText($model, $correctPercent);
    }




    public function getTestTextForDoctor($correctPercent){

        $model = DoctorAssessmentPrinciple::get();
        TranslatesCollection::translate($model, app()->getLocale());
        return $this->getText($model, $correctPercent);
    }



    private static function getText($model, $correctPercent){

        $text = "";
        foreach ($model as $v){
            if($correctPercent >= $v->from && $correctPercent <= $v->to) {
                $text = $v->general; break;
            }
        }

        return $text;
    }



    public function getTestResultTextForPharmacist($correctPercent){

        $model = PharmacistAssessmentPrinciple::get();
        TranslatesCollection::translate($model, app()->getLocale());

        $text = "";
        foreach ($model as $v){
            if($correctPercent >= $v->from && $correctPercent <= $v->to) {
                $text = $v->after_result; break;
            }
        }

        return $text;
    }




}
