<?php

namespace App\Http\Controllers\API;

use App\Helpers\TranslatesCollection;
use App\Interest;
use App\Http\Resources\InterestCollection;
use App\Http\Resources\InterestResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InterestAPIController extends Controller
{
    public function index()
    {
        $interest = Interest::getAll();
        TranslatesCollection::translate($interest, app()->getLocale());
        return new InterestCollection($interest);
    }
 
    public function show(Interest $interest)
    {
        TranslatesCollection::translate($interest, app()->getLocale());
        return new InterestResource($interest);
    }


}
