<?php

namespace App\Http\Controllers\API;


use App\UserProfile;
use App\WatchedDoctorVideoLesson;
use App\WatchedPharmacistVideoLesson;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserAPIController extends Controller
{


    public function index()
    {
        $user =  Auth::user();
        $profile = UserProfile::getProfileByID($user->id);

        if($profile && $user){
            $data = $this->getUserData($user, $profile);
            return response()->json(['data' => $data]);
        }else{
            return response(['Network does not exist'], 422);
        }


    }



    public function update(Request $request)
    {
        $user = Auth::user();
        $profile = UserProfile::getProfileByID($user->id);

        $validateArray = [
            'name' => 'required|max:255|min:8',
            'email' => 'required',
//            'profession_id' => 'required|exists:professions,id',
            'interest_id'  => 'required',
            'city' => 'required',
            'phone' => 'required',
            'place_of_work' => 'required|max:255',
        ];

        $validateArray = $this->checkEmail($request, $validateArray, Auth::user()->email);
//        $validateArray = $this->checkProfessionAndValidate($request, $validateArray);
        $validateArray = $this->checkPasswordAndValidate($request, $validateArray);
        if($userValidate = $this->check($request, $validateArray)) return $userValidate;

        $input = $request->all();


        if($profile->profession_id == UserProfile::isPharmacist){
            if($profile->profession_id != $input['profession_id']) {
                WatchedPharmacistVideoLesson::deleteByUser(Auth::id());
            }
        }elseif ($profile->profession_id == UserProfile::isDoctor){
            if($profile->profession_id != $input['profession_id'] || $profile->direction_id != $input['direction_id']) {
                WatchedDoctorVideoLesson::deleteByUser(Auth::id());
            }
        }


        if($request->password != null) {
            $input['password'] = bcrypt($request->password);
        }else{
            unset($input['password']);
        }

        $user->update($input);
        $profile->update($input);

//        $data = $this->getUserData($user, $profile);
        return response()->json([]);

    }



    public function getUserData($user, $profile){
        $data = [
            'profession' => $profile->profession_id,
            'interest' => $profile->interest_id
        ];

        if($profile->profession_id == UserProfile::isDoctor){
            $data['direction'] = $profile->directionName();
        }

        $data['fio'] = $user->name;
        $data['city'] = $profile->city;
        $data['email'] = $user->email;
        $data['telephone'] = $profile->phone;
        $data['place_of_work'] = $profile->place_of_work;

        return $data;
    }


    private function checkPasswordAndValidate(Request $request, Array $validaArray){
        if(isset($request->password) && $request->password != null){
            $validaArray['password'] =  'required|max:16|min:8';
            $validaArray['password_confirmation'] = 'required|same:password';
        }
        return $validaArray;
    }

    private function checkEmail(Request $request, Array $validaArray, $currentEmail){
        if($request->email != $currentEmail){
            $validaArray['email'] = 'email|max:255|unique:users';
        }
        return $validaArray;
    }
 

}
