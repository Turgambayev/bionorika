<?php


namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class PharmacovigilanceController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }
}
