<?php

namespace App\Http\Controllers;

use App\UserProfile;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected function check(Request $request, $validator){
        $validator = Validator::make($request->all(), $validator);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
    }

    protected function checkProfessionAndValidate(Request $request, Array $validaArray){
        if($request->profession_id == UserProfile::isDoctor){
            $validaArray['direction_id'] = 'required|exists:directions,id';
        }
        return $validaArray;
    }


}
