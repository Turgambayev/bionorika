<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.04.2020
 * Time: 13:24
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Question;
use App\UserQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnswerController extends Controller
{
    public function answer(Request $request)
    {

        if(!UserQuestion::where('user_id', Auth::user()->id)->exists()){
            $validateArray = $this->getValidation();
            if($res = $this->check($request, $validateArray)) return $res;

            foreach ($request->all() as $k => $v){
                $userQuestion = new UserQuestion();
                $userQuestion->createNew(intval(substr($k, -1)), $v, Auth::user()->id);
            }

            Auth::guard('api')->user()->token()->revoke();
            return response()->json([], 200);

        }else{
            return response(['Network does not exist'], 422);
        }

    }

    private static function getValidation(){

        $validateArray = [];
        $questions = Question::getAll();

        foreach ($questions as $k => $v) {
            $validateArray['question_'.$v->id] = 'required';}

        return $validateArray;

    }
}
