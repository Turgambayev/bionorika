<?php

namespace App;

use App\Traits\Testable;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use TCG\Voyager\Traits\Translatable;
use Carbon\Carbon;

class DoctorTest extends Model
{
    use Translatable;
    use Testable;

    protected $translatable = ['name', 'theme'];

    const isActive = 1;
    const isNotActive = 0;
    const maxBall = 45;
    const firstCycleBall = 10;
    const secondCycleBall = 15;
    const thirdCycleBall = 20;

    public static function getTestByDirection($direction_id){

        return self::join('doctor_video_lessons', 'doctor_video_lessons.id', '=', 'doctor_tests.doctor_video_lesson_id')
            ->join('doctor_video_lesson_directions', 'doctor_video_lesson_directions.doctor_video_lesson_id',
                '=', 'doctor_video_lessons.id')
            ->select('doctor_tests.id', 'doctor_tests.name', 'doctor_tests.attempts', 'doctor_tests.doctor_video_lesson_id')
            ->where('doctor_video_lesson_directions.direction_id', $direction_id)
            ->get();
    }


    public static function getStatus($lesson_id)
    {
        if ($model = self::where('doctor_video_lesson_id', $lesson_id)->first()){
            if (Carbon::createFromFormat('Y-m-d H:i:s' ,$model->from_time)->timestamp < Carbon::now()->timestamp &&
                Carbon::createFromFormat('Y-m-d H:i:s' ,$model->to_time)->timestamp > Carbon::now()->timestamp) {
                return self::isActive;
            } else return self::isNotActive;
        }else{
            return self::isNotActive;
        }
    }

    public static function getContent($user_id, $lesson_id){

        $model = self::where('doctor_video_lesson_id', $lesson_id)
            ->select('id' ,'name', 'theme', 'test_time', 'from_time', 'to_time', 'attempts')->first();
        $model->attempts_left = $model->attempts - DoctorScore::getTestAttempt($user_id, $model->id);
        $model->test_time = Carbon::createFromFormat('H:i:s' ,$model->test_time)->minute.' минут';
        $model->number_of_questions = DoctorTestQuestion::getCount($model->id);
        $model->test_is_valid = Date::parse($model->from_time)->format('j F Y г.').'  -  '.
            Date::parse($model->to_time)->format('j F Y г.');

        unset($model->from_time, $model->to_time);
        return $model;
    }







}
