<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class TextTranslation extends Model
{
    use Translatable;
    protected $translatable = ['value'];

    protected $fillable = [
        'key', 'value'
    ];
    
}
