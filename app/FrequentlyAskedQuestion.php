<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class FrequentlyAskedQuestion extends Model
{
    use Translatable;
    protected $translatable = ['question', 'answer'];

    public static function getAll(){
        return self::select('id', 'question', 'answer')
            ->orderBy('sort', 'ASC')
            ->get();
    }
    
}
