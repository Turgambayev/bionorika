<?php

namespace App\Policies;

use App\User;
use App\Questionnaire;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionnairePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any questionnaire.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the questionnaire.
     *
     * @param  App\User  $user
     * @param  App\Questionnaire  $questionnaire
     * @return bool
     */
    public function view(User $user, Questionnaire $questionnaire)
    {
        return false;
    }

    /**
     * Determine whether the user can create a questionnaire.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the questionnaire.
     *
     * @param  App\User  $user
     * @param  App\Questionnaire  $questionnaire
     * @return bool
     */
    public function update(User $user, Questionnaire $questionnaire)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the questionnaire.
     *
     * @param  App\User  $user
     * @param  App\Questionnaire  $questionnaire
     * @return bool
     */
    public function delete(User $user, Questionnaire $questionnaire)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the questionnaire.
     *
     * @param  App\User  $user
     * @param  App\Questionnaire  $questionnaire
     * @return bool
     */
    public function restore(User $user, Questionnaire $questionnaire)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the questionnaire.
     *
     * @param  App\User  $user
     * @param  App\Questionnaire  $questionnaire
     * @return bool
     */
    public function forceDelete(User $user, Questionnaire $questionnaire)
    {
        return false;
    }
}
