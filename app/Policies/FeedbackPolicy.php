<?php

namespace App\Policies;

use App\User;
use App\Feedback;
use Illuminate\Auth\Access\HandlesAuthorization;

class FeedbackPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any feedback.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the feedback.
     *
     * @param  App\User  $user
     * @param  App\Feedback  $feedback
     * @return bool
     */
    public function view(User $user, Feedback $feedback)
    {
        return false;
    }

    /**
     * Determine whether the user can create a feedback.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the feedback.
     *
     * @param  App\User  $user
     * @param  App\Feedback  $feedback
     * @return bool
     */
    public function update(User $user, Feedback $feedback)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the feedback.
     *
     * @param  App\User  $user
     * @param  App\Feedback  $feedback
     * @return bool
     */
    public function delete(User $user, Feedback $feedback)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the feedback.
     *
     * @param  App\User  $user
     * @param  App\Feedback  $feedback
     * @return bool
     */
    public function restore(User $user, Feedback $feedback)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the feedback.
     *
     * @param  App\User  $user
     * @param  App\Feedback  $feedback
     * @return bool
     */
    public function forceDelete(User $user, Feedback $feedback)
    {
        return false;
    }
}
