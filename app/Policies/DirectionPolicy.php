<?php

namespace App\Policies;

use App\User;
use App\Direction;
use Illuminate\Auth\Access\HandlesAuthorization;

class DirectionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any direction.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the direction.
     *
     * @param  App\User  $user
     * @param  App\Direction  $direction
     * @return bool
     */
    public function view(User $user, Direction $direction)
    {
        return false;
    }

    /**
     * Determine whether the user can create a direction.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the direction.
     *
     * @param  App\User  $user
     * @param  App\Direction  $direction
     * @return bool
     */
    public function update(User $user, Direction $direction)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the direction.
     *
     * @param  App\User  $user
     * @param  App\Direction  $direction
     * @return bool
     */
    public function delete(User $user, Direction $direction)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the direction.
     *
     * @param  App\User  $user
     * @param  App\Direction  $direction
     * @return bool
     */
    public function restore(User $user, Direction $direction)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the direction.
     *
     * @param  App\User  $user
     * @param  App\Direction  $direction
     * @return bool
     */
    public function forceDelete(User $user, Direction $direction)
    {
        return false;
    }
}
