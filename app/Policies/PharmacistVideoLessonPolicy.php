<?php

namespace App\Policies;

use App\User;
use App\PharmacistVideoLesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class PharmacistVideoLessonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\PharmacistVideoLesson  $pharmacistVideoLesson
     * @return bool
     */
    public function view(User $user, PharmacistVideoLesson $pharmacistVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can create a pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\PharmacistVideoLesson  $pharmacistVideoLesson
     * @return bool
     */
    public function update(User $user, PharmacistVideoLesson $pharmacistVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\PharmacistVideoLesson  $pharmacistVideoLesson
     * @return bool
     */
    public function delete(User $user, PharmacistVideoLesson $pharmacistVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\PharmacistVideoLesson  $pharmacistVideoLesson
     * @return bool
     */
    public function restore(User $user, PharmacistVideoLesson $pharmacistVideoLesson)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the pharmacistVideoLesson.
     *
     * @param  App\User  $user
     * @param  App\PharmacistVideoLesson  $pharmacistVideoLesson
     * @return bool
     */
    public function forceDelete(User $user, PharmacistVideoLesson $pharmacistVideoLesson)
    {
        return false;
    }
}
