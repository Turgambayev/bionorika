<?php

namespace App\Policies;

use App\User;
use App\Cup;
use Illuminate\Auth\Access\HandlesAuthorization;

class CupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any cup.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the cup.
     *
     * @param  App\User  $user
     * @param  App\Cup  $cup
     * @return bool
     */
    public function view(User $user, Cup $cup)
    {
        return false;
    }

    /**
     * Determine whether the user can create a cup.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the cup.
     *
     * @param  App\User  $user
     * @param  App\Cup  $cup
     * @return bool
     */
    public function update(User $user, Cup $cup)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the cup.
     *
     * @param  App\User  $user
     * @param  App\Cup  $cup
     * @return bool
     */
    public function delete(User $user, Cup $cup)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the cup.
     *
     * @param  App\User  $user
     * @param  App\Cup  $cup
     * @return bool
     */
    public function restore(User $user, Cup $cup)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the cup.
     *
     * @param  App\User  $user
     * @param  App\Cup  $cup
     * @return bool
     */
    public function forceDelete(User $user, Cup $cup)
    {
        return false;
    }
}
