<?php

namespace App\Policies;

use App\User;
use App\FrequentlyAskedQuestion;
use Illuminate\Auth\Access\HandlesAuthorization;

class FrequentlyAskedQuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @param  App\FrequentlyAskedQuestion  $frequentlyAskedQuestion
     * @return bool
     */
    public function view(User $user, FrequentlyAskedQuestion $frequentlyAskedQuestion)
    {
        return false;
    }

    /**
     * Determine whether the user can create a frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @param  App\FrequentlyAskedQuestion  $frequentlyAskedQuestion
     * @return bool
     */
    public function update(User $user, FrequentlyAskedQuestion $frequentlyAskedQuestion)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @param  App\FrequentlyAskedQuestion  $frequentlyAskedQuestion
     * @return bool
     */
    public function delete(User $user, FrequentlyAskedQuestion $frequentlyAskedQuestion)
    {
        return false;
    }

    /**
     * Determine whether the user can restore the frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @param  App\FrequentlyAskedQuestion  $frequentlyAskedQuestion
     * @return bool
     */
    public function restore(User $user, FrequentlyAskedQuestion $frequentlyAskedQuestion)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the frequentlyAskedQuestion.
     *
     * @param  App\User  $user
     * @param  App\FrequentlyAskedQuestion  $frequentlyAskedQuestion
     * @return bool
     */
    public function forceDelete(User $user, FrequentlyAskedQuestion $frequentlyAskedQuestion)
    {
        return false;
    }
}
