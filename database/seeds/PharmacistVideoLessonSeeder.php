<?php

use App\PharmacistVideoLesson;
use Illuminate\Database\Seeder;

class PharmacistVideoLessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PharmacistVideoLesson::class, 10)->create();
    }
}