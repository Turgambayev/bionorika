<?php

use App\Cup;
use Illuminate\Database\Seeder;

class CupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Cup::class, 10)->create();
    }
}