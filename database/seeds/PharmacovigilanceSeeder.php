<?php

use App\Pharmacovigilance;
use Illuminate\Database\Seeder;

class PharmacovigilanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pharmacovigilance::class, 10)->create();
    }
}